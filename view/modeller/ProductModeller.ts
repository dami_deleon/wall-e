import { Product } from "../data/models/Product";
import { MapValueCheck } from "../helpers/MapValueCheck";
import { notes2ExpertCorrelation } from "./ExpertCorrelationModeller";

export class ProductModeller {
  public static clientProduct2Product(clientProduct: MapValueCheck): Product {
    return new Product(
      clientProduct.nonEmptyStringOrFail("id"),
      clientProduct.nonEmptyStringOrFail("name"),
      clientProduct.boolOrFail("sellable"),
      clientProduct.boolOrFail("buyable"),
      clientProduct.numericOrFail("sell_price"),
      clientProduct.numericOrFail("buy_price"),
      clientProduct.numericOrFail("ptype_id"),
      clientProduct.numericOrFail("company_id"),
      clientProduct.dateOrFail("created"),
      clientProduct.dateOrFail("modified"),
      clientProduct.boolOrFail("is_service"),
      clientProduct.stringOrNull("notes"),
      notes2ExpertCorrelation(clientProduct.stringOrNull("notes")),
    );
  }
}
