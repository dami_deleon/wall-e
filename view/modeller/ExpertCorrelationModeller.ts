import { ExpertCorrelation } from "../data/models/ExpertCorrelation";

function notes2ExpertCorrelation(
  inputText: null | string,
): null | ExpertCorrelation {
  if (!inputText) return null;

  const regex = /(\w+):\s*(\d+)/g;
  let match;
  const result: Map<string, string> = new Map();

  while ((match = regex.exec(inputText))) {
    const key = match[1];
    const value = match[2];
    result.set(key, value);
  }

  if (result.size === 0) return null;

  const correlation: ExpertCorrelation = {};

  correlation.saleAccount = result.get("ven_cuenta");
  correlation.tax10Account = result.get("cta_iva");
  correlation.tax05Account = result.get("cta_iva5");
  correlation.tax00Account = result.get("cta_exe");
  correlation.pointAccount = result.get("cta_caja");
  correlation.saleCost = result.get("ven_centro");
  correlation.currency = result.get("moneda");

  if (Object.keys(correlation).length === 0) return null;
  return correlation;
}

function expertCorrelation2Notes(correlation: ExpertCorrelation): string {
  let text = "";
  if (correlation.saleAccount)
    text += `ven_cuenta: ${correlation.saleAccount}\n`;
  if (correlation.tax10Account)
    text += `cta_iva: ${correlation.tax10Account}\n`;
  if (correlation.tax05Account)
    text += `cta_iva5: ${correlation.tax05Account}\n`;
  if (correlation.tax00Account)
    text += `cta_exe: ${correlation.tax00Account}\n`;
  if (correlation.pointAccount)
    text += `cta_caja: ${correlation.pointAccount}\n`;
  if (correlation.saleCost) text += `ven_centro: ${correlation.saleCost}\n`;
  if (correlation.currency) text += `moneda: ${correlation.currency}\n`;

  return text;
}

function cleanNotes(inputText: null | string): string {
  if (!inputText) return "";
  const resultText = inputText.replace(
    /(\w+:\s*\d+\s*\n*|\w+:\s*\d+\s*\n*(.*?)(?=\w+:|$))/gs,
    "",
  );

  return resultText.trim();
}

export { notes2ExpertCorrelation, expertCorrelation2Notes, cleanNotes };
