import { DateTime } from "luxon";
import { Contact, DocumentTypeEnum } from "../data/models/Contact";
import { Product } from "../data/models/Product";
import { SalesDatum } from "../data/models/SaleDatum";
import { SaleInvoice } from "../data/models/SalesInvoice";
import { SalesPayment } from "../data/models/SalesPayment";
import { SalesProduct } from "../data/models/SalesProduct";
import { MapValueCheck } from "../helpers/MapValueCheck";
import { notes2ExpertCorrelation } from "./ExpertCorrelationModeller";
import { ProductModeller } from "./ProductModeller";

export class SalesInvoiceModeller {
  static client2SalesDatum(salesDatum: MapValueCheck): SalesDatum {
    const saleMap = new MapValueCheck(salesDatum.objectOrFail("Sale"));
    const contactRaw = salesDatum.objectOrNull("Contact");
    const salePaymentMap = salesDatum.arrayOrNull("SalesPaymentMethod");
    const salesProductMap = salesDatum.arrayOrFail("SalesProduct");

    const datum: SalesDatum = {
      invoice: this.clientMap2SaleInvoice(saleMap),
      product: salesProductMap.map((productRaw: object) =>
        this.clientSalesProductMap2SalesProduct(new MapValueCheck(productRaw)),
      ),
    } as SalesDatum;

    if (salePaymentMap) {
      datum.paymentMethod = salePaymentMap.map((paymentRaw: object) =>
        this.clientPaymentMap2SalesPayment(new MapValueCheck(paymentRaw)),
      );
    }

    if (contactRaw) {
      const contactMap = new MapValueCheck(contactRaw);
      if (contactMap.stringOrNull("id")) {
        datum.contact = this.clientContactMap2Client(contactMap);
      }
    }

    return datum;
  }

  private static clientContactMap2Client(contactMap: MapValueCheck): Contact {
    return new Contact(
      contactMap.stringOrNull("document_id") ?? "",
      contactMap.stringOrNull("document_type") === "ruc"
        ? DocumentTypeEnum.RUC
        : DocumentTypeEnum.CI,
      contactMap.stringOrNull("emails"),
      contactMap.stringOrNull("first_name"),
      contactMap.stringOrNull("gender"),
      contactMap.nonEmptyStringOrFail("id"),
      contactMap.stringOrNull("image"),
      contactMap.stringOrNull("invoicing_address"),
      contactMap.stringOrNull("invoicing_document_id"),
      contactMap.stringOrNull("invoicing_name"),
      contactMap.stringOrNull("invoicing_phone"),
      contactMap.stringOrNull("last_name"),
      contactMap.nonEmptyStringOrFail("name"),
      contactMap.stringOrNull("phones"),
      contactMap.stringOrNull("notes"),
      notes2ExpertCorrelation(contactMap.stringOrNull("notes")),
    );
  }

  private static clientMap2SaleInvoice(saleMap: MapValueCheck): SaleInvoice {
    return new SaleInvoice(
      saleMap.nonEmptyStringOrFail("agency_billing_code"),
      saleMap.nonEmptyStringOrFail("posale_billing_code"),
      saleMap.nonEmptyStringOrFail("agency_id"),
      saleMap.numericOrFail("amount"),
      saleMap.boolOrFail("approved"),
      saleMap.boolOrFail("billed"),
      saleMap.stringOrNull("billing_concept"),
      saleMap.nonEmptyStringOrFail("company_id"),
      saleMap.stringOrNull("contact_emails"),
      saleMap.stringOrNull("contact_id"),
      DateTime.fromJSDate(saleMap.dateOrFail("created"), {
        zone: "America/Asuncion",
      }).toJSDate(),
      saleMap.boolOrFail("credit"),
      saleMap.stringOrNull("doc_number"),
      saleMap.stringOrNull("document_type"),
      saleMap.nonEmptyStringOrFail("id"),
      saleMap.nonEmptyStringOrFail("invoice_number"),
      DateTime.fromJSDate(saleMap.dateOrFail("invoiced"), {
        zone: "America/Asuncion",
      }).toJSDate(),
      saleMap.numericOrFail("invoiced_amount"),
      DateTime.fromJSDate(saleMap.dateOrFail("issue_date"), {
        zone: "America/Asuncion",
      }).toJSDate(),
      saleMap.stringOrNull("notes"),
      saleMap.nonEmptyStringOrFail("paid"),
      saleMap.boolOrFail("send_invoice"),
      saleMap.nonEmptyStringOrFail("stamping_code"),
      saleMap.numericOrFail("total_tax_amount"),
      saleMap.nonEmptyStringOrFail("user_id"),
      saleMap.boolOrFail("void"),
    );
  }

  private static clientPaymentMap2SalesPayment(
    salesPaymentMap: MapValueCheck,
  ): SalesPayment {
    return new SalesPayment(
      salesPaymentMap.nonEmptyStringOrFail("sale_id"),
      salesPaymentMap.nonEmptyStringOrFail("payment_method_id"),
      salesPaymentMap.numericOrFail("amount"),
      salesPaymentMap.dateOrFail("created"),
      salesPaymentMap.dateOrFail("modified"),
      salesPaymentMap.boolOrFail("credit"),
      salesPaymentMap.boolOrFail("delayed"),
      salesPaymentMap.dateOrNull("pay_date"),
    );
  }

  private static clientSalesProductMap2SalesProduct(
    salesProduct: MapValueCheck,
  ): SalesProduct {
    return new SalesProduct(
      salesProduct.dateOrFail("created"),
      salesProduct.nonEmptyStringOrFail("id"),
      salesProduct.stringOrNull("notes"),
      salesProduct.numericOrFail("price"),
      salesProduct.numericOrFail("quantity"),
      salesProduct.nonEmptyStringOrFail("sale_id"),
      salesProduct.nonEmptyStringOrFail("tax_amount"),
      salesProduct.nonEmptyStringOrFail("tax_id"),
      salesProduct.nonEmptyStringOrFail("tax_rate"),
      this.clientProduct2Product(
        new MapValueCheck(salesProduct.objectOrFail("Product")),
      ),
    );
  }

  private static clientProduct2Product(clientProduct: MapValueCheck): Product {
    return ProductModeller.clientProduct2Product(clientProduct);
  }
}
