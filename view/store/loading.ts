import { defineStore } from "pinia";

// Define el store de carga (loading)
export const useLoadingStore = defineStore("loading", {
  state: () => ({
    loading: false,
  }),
  actions: {
    switchLoad(state: boolean) {
      this.loading = state;
    },
  },
});

export function setLoading(state: boolean): void {
  useLoadingStore().switchLoad(state);
}

export const isLoading = () => useLoadingStore().loading;
