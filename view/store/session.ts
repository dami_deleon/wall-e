import { defineStore } from "pinia";

export const useSessionStore = defineStore({
  id: "session",
  state: () => ({
    session: "",
  }),
  actions: {
    save(sessionId: string) {
      this.session = sessionId;
    },
  },
});
