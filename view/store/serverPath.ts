import { defineStore } from "pinia";

export const useServerPathStore = defineStore({
  id: "serverPort",
  state: () => ({
    serverPath: "",
  }),
  actions: {
    save(path: string) {
      this.serverPath = path;
    },
  },
});

export function setApiPort(path: string): void {
  useServerPathStore().save(path);
}
