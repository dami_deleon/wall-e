export class ValueCheck {
  static nonEmptyStringOrNull(value: any): string | null {
    if (typeof value === "string" && value.length > 0) {
      return value;
    }
    if (typeof value === "number") {
      return "" + value;
    }
    return null;
  }

  static dateOrNull(value: any): Date | null {
    if (value instanceof Date) {
      return value;
    }
    if (typeof value === "string") {
      const d = new Date(value);
      if (!isNaN(d.getTime())) return d; // Retornar solamente is la fecha fue parseada exitosamente
    }
    return null;
  }

  static stringOrNull(value: any): string | null {
    if (typeof value === "string") {
      return value;
    }
    if (typeof value === "number") {
      return "" + value;
    }
    return null;
  }

  static textFromException(e: any | undefined): string {
    if (e === undefined) {
      return "error indefinido";
    }
    if (e instanceof Error) {
      return e.message;
    }
    return "" + e;
  }
}
