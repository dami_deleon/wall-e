import { ValueCheck } from "./ValueCheck";

export class MapValueCheck {
  public map: Map<string, any>;

  constructor(object: object | null) {
    this.map = new Map<string, any>(Object.entries(object ?? {}));
  }

  public get(name: string): any | null | undefined {
    return this.map.get(name);
  }

  public boolOrNull(name: string): boolean | null {
    const v = this.map.get(name);
    if (v === undefined) return null;
    return v === true;
  }

  public boolOrFail(name: string): boolean {
    const data = this.boolOrNull(name);
    if (data === null) {
      throw new Error('Valor "' + name + '" es invalido (bool)');
    }
    return data;
  }

  public numericOrNull(name: string): number | null {
    const v = this.map.get(name);
    if (v !== undefined) {
      if (typeof v === "number") return v;
      if (typeof v === "string") {
        const f = Number.parseFloat(v);
        if (!isNaN(f)) return f;
      }
    }
    return null;
  }

  public numericOrFail(name: string): number {
    const num = this.numericOrNull(name);
    if (num === null) {
      throw new Error('Valor "' + name + '" es invalido (num)');
    }
    return num;
  }

  public arrayOrNull(name: string): any[] | null {
    const value = this.map.get(name);
    if (value !== undefined && Array.isArray(value)) {
      return value;
    }
    return null;
  }

  public objectOrNull(name: string): object | null {
    const value = this.map.get(name);
    if (value !== undefined) {
      if (typeof value === "object") return value;
    }
    return null;
  }

  public arrayOrFail(name: string): any[] {
    const value = this.arrayOrNull(name);
    if (value !== null) return value;
    throw new Error('Valor "' + name + '" es invalido (array)');
  }
  public objectOrFail(name: string): object {
    const value = this.objectOrNull(name);
    if (value !== null) return value;
    throw new Error('Valor "' + name + '" es invalido (object)');
  }

  public nonEmptyStringOrFail(name: string): string {
    if (this.map.has(name)) {
      const value = ValueCheck.nonEmptyStringOrNull(this.map.get(name));
      if (value !== null) return value;
    }
    throw new Error('Valor "' + name + '" es invalido (nonEmptyString)');
  }

  public dateOrNull(name: string): Date | null {
    const value = this.map.get(name);
    return value === undefined ? null : ValueCheck.dateOrNull(value);
  }

  public dateOrFail(name: string): Date {
    const value = this.dateOrNull(name);
    if (value !== null) return value;
    throw new Error('Valor "' + name + '" es invalido (dateOrFail)');
  }

  public stringOrNull(name: string): string | null {
    if (this.map.has(name)) {
      return ValueCheck.stringOrNull(this.map.get(name));
    }
    return null;
  }

  static textFromException(e: any | undefined): string {
    return ValueCheck.textFromException(e);
  }
}
