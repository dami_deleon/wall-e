import { DateTime } from "luxon";

export class CacheStore {
  constructor(
    private key: string,
    private expirationMinutes: number,
  ) {}

  private getExpiryTime() {
    const now = DateTime.now();
    return now.plus({ minutes: this.expirationMinutes }).toMillis();
  }

  public setItem(data: any) {
    const expirationTime = this.getExpiryTime();
    const item = { data, _cacheExpiresAt: expirationTime };
    sessionStorage.setItem(this.key, JSON.stringify(item));
  }

  public getItem() {
    const item = sessionStorage.getItem(this.key);
    if (item) {
      const parsedItem = JSON.parse(item);
      if (parsedItem._cacheExpiresAt >= DateTime.now().toMillis()) {
        return parsedItem.data;
      } else {
        this.removeItem();
      }
    }
    return null;
  }

  public removeItem() {
    sessionStorage.removeItem(this.key);
  }
}
