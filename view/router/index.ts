import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../views/Home.vue";
import BimsExporter from "../views/BimsToExpert.vue";
import LiveconnectReport from "../views/LiveconnectReport.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/bims-exporter",
    name: "BimsExporter",
    component: BimsExporter,
  },
  {
    path: "/liveconnect-report",
    name: "LiveconnectReport",
    component: LiveconnectReport,
  },
];

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
