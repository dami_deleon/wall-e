import { BackendApiClient } from "./BackendApiClient";
import { useServerPathStore } from "../store/serverPath";
import { CacheStore } from "../helpers/CacheStore";
import { MapValueCheck } from "../helpers/MapValueCheck";

const sesionSlug = "liveconnect-token";
export class Liveconnect extends BackendApiClient {
  private static clientEnpoint: string = "/liveconnect";

  private constructor(apiPath: string) {
    super(apiPath + Liveconnect.clientEnpoint);
  }

  public static allocClientOrFail(): Liveconnect {
    const serverPath = useServerPathStore().serverPath;
    if (serverPath.length < 1) throw Error("No se configuró el servidor");
    return new Liveconnect(serverPath);
  }

  public async connect(user: string, password: string) {
    try {
      const { data } = await this.client.post("/connect", {
        user,
        password,
      });

      const dataRaw = new MapValueCheck(data);

      const dataAPI = new MapValueCheck(dataRaw.objectOrFail("data"));
      const token = new MapValueCheck(dataAPI.objectOrFail("token"));
      const report = dataAPI.arrayOrFail("report");

      const cache = getSessionCache();
      cache.setItem(token.stringOrNull("auth"));

      return report as Array<{
        name: string;
        channel: string;
        dateTime: string;
        prevText: string;
      }>;
    } catch (error) {
      console.error(error);
    }
  }

  public async refreshReport() {
    try {
      const sessionCache = getSessionCache();

      const { data } = await this.client.get("/report", {
        headers: {
          Authorization: sessionCache.getItem(),
        },
      });

      //console.log(data);

      const dataRaw = new MapValueCheck(data);
      const dataAPI = new MapValueCheck(dataRaw.objectOrFail("data"));
      const report = dataAPI.arrayOrFail("report");

      return report as Array<{
        name: string;
        channel: string;
        dateTime: string;
        prevText: string;
      }>;
    } catch (err) {
      console.error(err);
    }
  }
}

function getSessionCache() {
  return new CacheStore(sesionSlug, 60);
}
