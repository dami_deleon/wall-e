import axios, {
  type AxiosError,
  type AxiosInstance,
  type AxiosRequestConfig,
  type AxiosResponse,
} from "axios";

export interface HttpInstance extends AxiosInstance {
  aditional?: object;
}

export interface HttpResponse extends AxiosResponse {
  aditional?: object;
}

export interface HttpRequestConfig extends AxiosRequestConfig {
  aditional?: object;
}

export interface HttpError extends AxiosError {
  aditional?: object;
}

export default class HttpClient {
  public readonly instance: HttpInstance;
  constructor(config: HttpRequestConfig) {
    config.headers = {
      "Content-Type": "application/json",
    };
    this.instance = axios.create(config);
  }

  public async post(
    endpoint: string,
    data?: object,
    requestConfig?: HttpRequestConfig,
  ): Promise<HttpResponse> {
    return await this.instance.post(endpoint, data, requestConfig);
  }

  public async get(
    endpoint: string,
    requestConfig?: HttpRequestConfig,
  ): Promise<HttpResponse> {
    return await this.instance.get(endpoint, requestConfig);
  }

  public async put(
    endpoint: string,
    data?: object,
    requestConfig?: HttpRequestConfig,
  ): Promise<HttpResponse> {
    return await this.instance.put(endpoint, data, requestConfig);
  }

  public async patch(
    endpoint: string,
    data?: object,
    requestConfig?: HttpRequestConfig,
  ): Promise<HttpResponse> {
    return await this.instance.patch(endpoint, data, requestConfig);
  }

  public async delete(
    endpoint: string,
    requestConfig?: HttpRequestConfig,
  ): Promise<HttpResponse> {
    return await this.instance.delete(endpoint, requestConfig);
  }

  public isHttpError(error: unknown): boolean {
    return axios.isAxiosError(error);
  }
}
