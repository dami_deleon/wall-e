import HttpClient, { HttpResponse } from "./HttpClient";
import md5 from "crypto-js/md5";
import { DateTime } from "luxon";
import { MapValueCheck } from "../helpers/MapValueCheck";
import { SalesDatum } from "../data/models/SaleDatum";
//import { ExpertRows } from "../data/models/ExpertRow";
import { SalesInvoiceModeller } from "../modeller/SalesInvoiceModeller";
import { Product } from "../data/models/Product";
import { ProductModeller } from "../modeller/ProductModeller";
import { CacheStore } from "../helpers/CacheStore";
import {
  cleanNotes,
  expertCorrelation2Notes,
} from "../modeller/ExpertCorrelationModeller";
import { ExpertCorrelation } from "../data/models/ExpertCorrelation";
import { Contact, DocumentTypeEnum } from "../data/models/Contact";

export type loginData = {
  user: string;
  password: string;
  tenant?: string;
};

export class Bims {
  private apiUrl: string = "https://bims.app/api";
  private client: HttpClient;
  //private tenant: string;

  constructor() {
    this.client = new HttpClient({
      baseURL: this.apiUrl,
      headers: {
        "Content-Type": "application/json",
      },
    });
  }

  public async initSession(
    user: string,
    password: string,
    tenant: string,
  ): Promise<string> {
    const response = await this.client.post("users/login", {
      user: user,
      password: md5(password).toString(),
      tenant: tenant,
    });

    if (!response.data) {
      throw new Error("Error de conexión");
    }

    const rawData = new MapValueCheck(response.data);
    const status = rawData.nonEmptyStringOrFail("status");
    if (status === "error") {
      throw Error(rawData.stringOrNull("message") ?? "Error inesperado");
    }

    const clientData = new MapValueCheck(rawData.objectOrFail("data"));
    const clientSession = new MapValueCheck(clientData.objectOrFail("Session"));
    //this.tenant = tenant;
    return clientSession.nonEmptyStringOrFail("id");
  }

  public async getInvoices(
    sessionId: string,
    dateFrom: Date,
    dateTo: Date,
  ): Promise<SalesDatum[]> {
    let page = 1;
    const limit = 20;
    let hasMore = true;

    const list: SalesDatum[] = [];

    while (hasMore) {
      const response = await this.requestSales(
        sessionId,
        dateFrom,
        dateTo,
        page,
        limit,
      );
      const responseData = new MapValueCheck(response.data);
      const data = responseData.arrayOrNull("data");
      const status = responseData.nonEmptyStringOrFail("status");
      if (status === "error") {
        throw Error(responseData.stringOrNull("message") ?? "Error inesperado");
      }

      if (!data) throw Error("No se pudo leer datos");

      if (data.length === 0)
        throw Error("La consulta no arrojo ningún resultado");

      if (data && data.length === limit) {
        data.map((saleData) => {
          const saleDatumRawData = new MapValueCheck(saleData);
          list.push(SalesInvoiceModeller.client2SalesDatum(saleDatumRawData));
        });
        page++;
      } else {
        data.map((saleData) => {
          const saleDatumRawData = new MapValueCheck(saleData);
          list.push(SalesInvoiceModeller.client2SalesDatum(saleDatumRawData));
        });
        hasMore = false;
      }
    }

    return list;
  }

  public async getProducts(sessionId: string): Promise<Product[]> {
    let page = 1;
    const limit = 20;
    let hasMore = true;

    const list: Product[] = [];

    while (hasMore) {
      const response = await this.requestProducts(sessionId, page, limit);
      const responseData = new MapValueCheck(response.data);
      const dataList = responseData.arrayOrNull("data");

      const status = responseData.nonEmptyStringOrFail("status");
      if (status === "error") {
        throw Error(responseData.stringOrNull("message") ?? "Error inesperado");
      }

      if (!dataList) throw Error("No se pudo leer datos");

      if (dataList.length === 0)
        throw Error("La consulta no arrojo ningún resultado");

      if (dataList.length === limit) {
        dataList.map((dataItem) => {
          const rawData = new MapValueCheck(dataItem);
          const clientProduct = new MapValueCheck(
            rawData.objectOrFail("Product"),
          );
          list.push(ProductModeller.clientProduct2Product(clientProduct));
        });
        page++;
      } else {
        dataList.map((dataItem) => {
          const rawData = new MapValueCheck(dataItem);
          const clientProduct = new MapValueCheck(
            rawData.objectOrFail("Product"),
          );
          list.push(ProductModeller.clientProduct2Product(clientProduct));
        });
        hasMore = false;
      }
    }

    return list;
  }

  static getProductMapCodeOrNull(
    tenant: string,
    product: Product,
  ): string | null {
    const mapCache = new CacheStore(`${tenant}-p${product.id}`, 2800);
    return mapCache.getItem() as string;
  }

  public async saveProductCorrelation(
    sessionId: string,
    product: Product,
    productCorrelation: ExpertCorrelation,
  ): Promise<boolean> {
    const notesCleaned = cleanNotes(product.notes) + "\n" ?? "";
    const correlationText = expertCorrelation2Notes(productCorrelation);

    const response = await this.client.post(`/products?sid=${sessionId}`, {
      Product: {
        id: parseInt(product.id, 10),
        notes: `${notesCleaned}${correlationText}`,
      },
    });

    const dataRaw = new MapValueCheck(response.data);

    const code = dataRaw.numericOrFail("code");

    return code === 200;
  }

  public async saveContactCorrelation(
    sessionId: string,
    contact: Contact,
    correlation: ExpertCorrelation,
  ): Promise<boolean> {
    const notesCleaned = cleanNotes(contact.notes);
    const correlationText = expertCorrelation2Notes(correlation);

    const response = await this.client.post(`/contacts?sid=${sessionId}`, {
      Contact: {
        id: parseInt(contact.id, 10),
        name: contact.name,
        document_id: contact.documentId,
        document_type:
          contact.documentType === DocumentTypeEnum.CI ? "ci" : "ruc",
        phones: contact.phones,
        emails: contact.emails,
        notes: `${notesCleaned}\n${correlationText}`,
      },
    });

    const dataRaw = new MapValueCheck(response.data);

    const code = dataRaw.numericOrFail("code");

    return code === 200;
  }

  private requestSales(
    sessionId: string,
    dateFrom: Date,
    dateTo: Date,
    page = 1,
    limit = 10,
  ): Promise<HttpResponse> {
    return this.client.get("/sales", {
      params: {
        sid: sessionId,
        from: DateTime.fromJSDate(dateFrom).toFormat("yyyy-MM-dd"),
        to: DateTime.fromJSDate(dateTo).toFormat("yyyy-MM-dd"),
        offset: (page - 1) * limit,
        limit: limit,
      },
    });
  }

  private requestProducts(
    sessionId: string,
    page = 1,
    limit = 10,
  ): Promise<HttpResponse> {
    return this.client.get("/products", {
      params: {
        sid: sessionId,
        offset: (page - 1) * limit,
        limit: limit,
      },
    });
  }
}
