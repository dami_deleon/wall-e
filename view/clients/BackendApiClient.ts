import HttpClient from "./HttpClient";

export class BackendApiClient {
  public client: HttpClient;

  constructor(readonly apiURL: string) {
    this.client = new HttpClient({
      baseURL: apiURL,
      headers: {
        "Content-Type": "application/json",
      },
    });
  }

  public getClient(): HttpClient {
    return this.client;
  }
}
