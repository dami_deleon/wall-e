/**
 * Representa un registro de comprobante de venta.
 */
export type ExpertSale = {
  /** Tipo de Impuesto. */
  ven_tipimp: string; // Posibles valores: 'I'

  /** Importe Gravado al 5%. */
  ven_gra05?: number;

  /** Importe de IVA al 5%. */
  ven_iva05?: number;

  /** Inciso correspondiente en el formulario por Gravada al 5%. */
  ven_disg05?: null | string; // Posibles valores: 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'

  /** Código de Cuenta Contable para Importe Gravado al 5%. */
  cta_iva05?: null | string; // El código de cuenta debe pertenecer a la Base de Datos de Destino

  /** Rubro correspondiente en el formulario por gravada al 10%. */
  ven_rubgra?: number;

  /** Rubro correspondiente en el formulario por Gravada al 5%. */
  ven_rubg05?: number;

  /** Inciso correspondiente en el formulario por importe exento. */
  ven_disexe: string; // Posibles valores: 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'

  /** Número de Comprobante. */
  ven_numero: string;

  /** Número de Control de Asiento Generado. */
  ven_imputa: number; // Valor por defecto: 0

  /** Sucursal a la que corresponde el movimiento. */
  ven_sucurs: string;

  /** Generar. */
  generar: number; // Valor por defecto: 0

  /** Forma de Pago. */
  form_pag: string; // Posibles valores: 'Crédito', 'Contado'

  /** Código Centro de Costo. */
  ven_centro: string; // El código debe pertenecer a la Base de Datos de Destino

  /** RUC del cliente. */
  ven_clie: string;

  /** Cuenta Contable por Importe Gravado al 10%. */
  ven_cuenta: string; // La cuenta debe pertenecer a la Base de Datos de Destino

  /** Nombre del Cliente. */
  ven_clinom: string;

  /** Tipo de Comprobante. */
  ven_tipofa: string; // Posibles valores: 'Comprob de Vta', 'Contado', 'Crédito', 'Boleta', 'Factura', 'Nota de Crédito', 'Nota de Débito', 'Retencion Iva', 'Retencion Renta', 'Retencio 2051', 'Tickets', 'Factura Export', 'Recibo', 'Anulado'

  /** Fecha del Comprobante. */
  ven_fecha: Date;

  /** Importe Total del Comprobante. */
  ven_totfac: number;

  /** Importe Exento. */
  ven_exenta: number;

  /** Importe Gravado al 10%. */
  ven_gravad: number;

  /** Total IVA (IVA al 10%). */
  ven_iva: number;

  /** Importe Retenido. */
  ven_retenc: number;

  /** Campo Auxiliar. */
  ven_aux: number; // Valor por defecto: 0

  /** Número de control de asiento. */
  ven_ctrl: number; // Valor por defecto: 0

  /** Concepto del Comprobante. */
  ven_con: string;

  /** Cantidad de Cuotas. */
  ven_cuota: number;

  /** Fecha del Primer Vencimiento. */
  ven_fecven: Date;

  /** Cantidad de días entre Vencimiento de Cuotas. */
  cant_dias: number;

  /** Origen del Comprobante. */
  origen: string; // Posibles valores: 'LI' (Libro de Venta)

  /** Factor de cambio utilizado en el comprobante si fue con moneda extranjera. */
  cambio?: number;

  /** Importe Total del Comprobante en Moneda extranjera. */
  valor?: number;

  /** Código Moneda en que se realizó el comprobante. */
  moneda: string; // Utilizar Código de Monedas de la Base de Datos Destino

  /** Exento en guaraníes. */
  exen_dolar: number; // Valor por defecto: 0

  /** Concepto del Comprobante. */
  concepto: string;

  /** Cuenta Contable por Importe IVA al 10%. */
  cta_iva: string; // La cuenta debe pertenecer a la Base de Datos de Destino

  /** Cuenta Contable por Importe del comprobante cuando es Contado/Crédito. */
  cta_caja: string; // La cuenta debe pertenecer a la Base de Datos de Destino

  /** Desde Número de Ticket. */
  tkdesde?: number;

  /** Hasta Número de Ticket. */
  tkhasta?: number;

  /** Número de Caja donde se realizó el ticket. */
  caja?: number;

  /** Inciso correspondiente en el formulario por importe Gravado al 10%. */
  ven_disgra: string; // Posibles valores: 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'

  /** Define si es descuento o devolución cuando el comprobante es nota de crédito. */
  forma_devo: number; // Posibles valores: 1 (Devolución), 2 (Descuento)

  /** No se Usa. */
  ven_cuense: null; // Vacío

  /** Indica si el Factura o Comprobante está anulado o no. */
  anular: 1 | 0; // Posibles valores: 1 (Anulado), 0 (Vigente)

  /** No se Usa. */
  reproceso: null; // Vacío

  /** Cuenta Contable por Importe Exento. */
  cuenta_exe: string; // La cuenta debe pertenecer a la Base de Datos de Destino

  /** Código de Usuario. */
  usu_ide: number; // Valor por defecto: 0

  /** Número de Timbrado. */
  rucvennrotim?: number;

  /** Valor 0 para asiento consolidado. */
  clieasi: number;

  /** Tipo de renta para IRP. */
  ventirptip?: 1 | 2 | 3; // Valores posibles: 1 al 3

  /** Gravada. */
  ventirpgra?: number;

  /** Exenta. */
  ventirpexe?: number;

  /** Si es IRPC. */
  irpc: 1 | 0; // Valor 0 (falso) o valor 1 (verdadero)

  /** Si es IVA Simplificado. */
  ivasimplificado: 0 | 1; // Valor 0 (falso) o valor 1 (verdadero)

  /** Número de factura asociada a la nota de crédito. */
  nroFacAso?: null | string; // Formato: xxx-xxx-xxxxxxx

  /** Número de timbrado de la factura asociado de la nota de crédito. */
  TimFacAso: number; // Valor por defecto: 0
};
