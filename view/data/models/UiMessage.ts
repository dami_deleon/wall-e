import { DateTime } from "luxon";

export interface UiMessage {
  title?: string;
  message: string;
  time?: DateTime;
}
