import { Contact } from "./Contact";
import { SaleInvoice } from "./SalesInvoice";
import { SalesPayment } from "./SalesPayment";
import { SalesProduct } from "./SalesProduct";

export type SalesDatum = {
  invoice: SaleInvoice;
  product: SalesProduct[];
  contact?: Contact;
  paymentMethod?: SalesPayment[];
};
