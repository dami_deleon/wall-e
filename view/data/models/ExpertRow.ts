export type ExpertRows = {
  ven_tipimp: string; // I
  ven_gra05: number; // 0
  ven_iva05: number; // 0
  ven_disg05: string; // null
  cta_iva05: string; // null
  ven_rubgra: number; // 1
  ven_rubg05: number; // 0
  ven_disexe: string | null; // null
  ven_numero: string; // 001-001-0000678
  ven_imputa: number; // 0
  ven_sucurs: string; // 01
  generar: number; // 0
  form_pag: string; // Contado
  ven_centro: string; // null
  ven_clie: string; // 7216850-1
  ven_cuenta: string; // 41231
  ven_clinom: string; // Esteban Gomez OcampoFactura
  ven_tipofa: string; // 29/08/2023
  ven_fecha: Date; // 52500
  ven_totfac: number; // 0
  ven_exenta: number; // 47727
  ven_gravad: number; // 4773
  ven_iva: number; // 0
  ven_retenc: number; // 0
  ven_aux: number; // 0
  ven_ctrl: string; // Venta s/ Factura Nº 001-001-0000678
  ven_con: number; // 0
  ven_cuota: number; // 0
  ven_fecven: number; // 0
  cant_dias: number; // LI
  origen: string; // 0
  cambio: number; // 0
  valor: number; // 0
  moneda: string; // Venta s/ Factura Nº 001-001-0000678
  exen_dolar: string; // 21231
  concepto: string; // 111101
  cta_iva: string; // 0
  cta_caja: string; // 0
  tkdesde: string; // 0
  tkhasta: string; // A
  caja: string; // 0
  ven_disgra: string; // null
  forma_devo: string | null; // 0
  ven_cuense: number; // null
  anular: string; // null
  reproceso: string; // 0
  cuenta_exe: string; // 0
  usu_ide: string; // 0
  rucvennrotim: string; // 16112940
  clieasi: number; // 0
  ventirptip: number; // 0
  ventirpgra: number; // 0
  ventirpexe: number; // 0
  irpc: number; // 0
  ivasimplificado: number; // 0
  nroFacAso: number | null; // null
  TimFacAso: number | null; // null
};
