export class SalesPayment {
  constructor(
    public saleId: string, // "2369",
    public paymentMethodId: string, // "21",
    public amount: number, // "52500",
    public created: Date, // "2023-08-29 10:11:00",
    public modified: Date, // "2023-08-29 10:11:00",
    public credit: boolean, // false,
    public delayed: boolean, // false,
    public payDate: null | Date, // "2023-08-29 00:00:00",
  ) {}
}
