export class CommonObject {
  constructor(
    public id: string,
    public label: string,
    public createdAt: Date | null,
    public updatedAt: Date | null,
  ) {}
}
