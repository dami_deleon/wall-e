import { Product } from "./Product";

export class SalesProduct {
  constructor(
    public created: Date,
    public id: string,
    public notes: null | string,
    public price: number,
    public quantity: number,
    public saleId: string,
    public taxAmount: string,
    public taxId: string,
    public taxRate: string,
    public product: Product,
  ) {}
}
