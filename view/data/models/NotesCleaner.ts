export interface NotesCleaner {
  getCleanNotes(): null | string;
}
