import { CommonObject } from "./CommonModel";
import { ExpertCorrelation } from "./ExpertCorrelation";

export class Product extends CommonObject implements ExpertCorrelation {
  public saleAccount?: string | null;
  public tax10Account?: string | null;
  public tax05Account?: string | null;
  public tax00Account?: string | null;
  public pointAccount?: string | null;
  public saleCost?: string | null;
  public currency?: string | null;

  constructor(
    public id: string,
    public name: string,
    public sellable: boolean,
    public buyable: boolean,
    public sellPrice: number,
    public buyPrice: number,
    public pTypeId: null | number,
    public companyId: number,
    public created: Date,
    public modified: Date,
    public isService: boolean,
    public notes: string | null,
    expertCode: ExpertCorrelation | null,
  ) {
    super(id, name, created, modified);
    this.saleAccount = expertCode?.saleAccount;
    this.tax10Account = expertCode?.tax10Account;
    this.tax05Account = expertCode?.tax05Account;
    this.tax00Account = expertCode?.tax00Account;
    this.pointAccount = expertCode?.pointAccount;
    this.currency = expertCode?.currency;
  }
}
