import { cleanNotes } from "../../modeller/ExpertCorrelationModeller";
import { CommonObject } from "./CommonModel";
import { ExpertCorrelation } from "./ExpertCorrelation";
import { NotesCleaner } from "./NotesCleaner";

export enum DocumentTypeEnum {
  CI = "ci",
  RUC = "ruc",
}
export class Contact
  extends CommonObject
  implements ExpertCorrelation, NotesCleaner
{
  public saleAccount?: string | null | undefined;
  public tax10Account?: string | null | undefined;
  public tax05Account?: string | null | undefined;
  public tax00Account?: string | null | undefined;
  public pointAccount?: string | null | undefined;
  public currency?: string | null | undefined;
  public saleCost?: string | null | undefined;

  constructor(
    public documentId: string,
    public documentType: DocumentTypeEnum,
    public emails: null | string,
    public firstName: null | string,
    public gender: null | string,
    public id: string,
    public image: null | string,
    public invoicingAddress: null | string,
    public invoicingDocumentId: null | string,
    public invoicingName: null | string,
    public invoicingPhone: null | string,
    public lastName: null | string,
    public name: string,
    public phones: null | string,
    public notes: null | string,
    expertCode: ExpertCorrelation | null,
  ) {
    super(id, name, null, null);
    this.saleAccount = expertCode?.saleAccount;
    this.tax10Account = expertCode?.tax10Account;
    this.tax05Account = expertCode?.tax05Account;
    this.tax00Account = expertCode?.tax00Account;
    this.pointAccount = expertCode?.pointAccount;
    this.currency = expertCode?.currency;
    this.saleCost = expertCode?.saleCost;
  }

  getCleanNotes(): string | null {
    return cleanNotes(this.notes);
  }
}
