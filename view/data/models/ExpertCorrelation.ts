export interface ExpertCorrelation {
  saleAccount?: string | null; // ven_cuenta
  tax10Account?: string | null; // cta_iva
  tax05Account?: string | null; // cta_iva5
  tax00Account?: string | null; // cta_exe
  pointAccount?: string | null; // cta_caja
  saleCost?: string | null; // ven_centro
  currency?: string | null; // moneda
}
