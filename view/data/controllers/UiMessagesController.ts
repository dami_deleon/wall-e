import { UiMessage } from "../models/UiMessage";

export interface UiMessageController {
  newMessage(message: UiMessage): void;
}
