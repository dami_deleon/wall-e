# Proyecto Wall-E
## Descripción

El proyecto Wall-E tiene como objetivo principal desarrollar una solución que permita la lectura de datos desde el sistema BIMS y la generación de un archivo Excel (.xls) que sea compatible con el sistema EXPERT para su importación.
## Requisitos

- **Usuario de BIMS**: Se requiere acceso al sistema BIMS para obtener los datos necesarios para la generación del archivo Excel.

- **Acceso a EXPERT:** Es esencial contar con acceso al sistema EXPERT para garantizar la compatibilidad del archivo generado y realizar las importaciones de datos de manera efectiva.