import { rmSync } from "node:fs";
import { defineConfig } from "vite";
import { fileURLToPath, URL } from "url";
import vue from "@vitejs/plugin-vue";
import electron from "vite-plugin-electron";
import pkg from "./package.json";

rmSync("dist-app", { recursive: true, force: true });

const isDevelopment =
  process.env.NODE_ENV === "development" || !!process.env.VSCODE_DEBUG;
const isProduction = process.env.NODE_ENV === "production";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    electron([
      {
        // Main-Process entry file of the Electron App.
        entry: "app/index.ts",
        onstart(options) {
          if (process.env.VSCODE_DEBUG) {
            console.log(
              /* For `.vscode/.debug.script.mjs` */ "[startup] Electron App",
            );
          } else {
            options.startup();
          }
        },
        vite: {
          build: {
            sourcemap: isDevelopment,
            minify: isProduction,
            outDir: "dist-app/",
            rollupOptions: {
              external: Object.keys(
                "dependencies" in pkg ? pkg.dependencies : {},
              ),
            },
          },
        },
      },
    ]),
  ],
  base: "",
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./view", import.meta.url)),
    },
  },
  server: {
    hmr: {
      overlay: false,
    },
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@import "bootstrap/scss/bootstrap";`,
      },
    },
  },
  // Importa el archivo JavaScript de Bootstrap
  optimizeDeps: {
    include: ["bootstrap"],
  },
});
