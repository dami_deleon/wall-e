import { Request } from "express";
import jwt, { SignOptions, VerifyOptions } from "jsonwebtoken";
import { DateTime } from "luxon";
import { Database, toDbDate } from "../utils/Db";
import * as crypto from "crypto";
import { MapValueCheck } from "../utils/MapValueCheck";
import { config } from "dotenv";

config();

const algorithm = "aes-256-cbc";
const passphrase = process.env.WALLE_PASSPHRASE;
const keyLength = 32;
const constantKey = crypto.scryptSync(passphrase, "retepu", keyLength);

interface TokenPayload {
  user: string;
  app: string;
}

export class Token {
  private static readonly secretKey = "secreto-purete";
  private dataToSave: any;
  private readonly algorithm = algorithm;
  private static readonly key: Buffer = constantKey;
  private dataEncripted: string;

  constructor(
    readonly value: string,
    readonly expiration: DateTime,
    readonly payload: TokenPayload,
  ) {}

  public getEncriptedData(): string | undefined {
    return this.dataEncripted;
  }

  public setDataToSave(dataToSave: any) {
    this.dataToSave = dataToSave;
  }

  private static generateIV() {
    return crypto.randomBytes(16);
  }

  static create(payload: TokenPayload, options?: SignOptions): Token {
    const tokenOptions: SignOptions = {
      expiresIn: "1h",
      algorithm: "HS256",
      ...options,
    };

    const value = jwt.sign(payload, this.secretKey, tokenOptions);
    const decodedToken = jwt.decode(value) as { exp?: number };

    return new Token(
      value,
      decodedToken.exp
        ? DateTime.fromSeconds(decodedToken.exp)
        : DateTime.local(),
      payload,
    );
  }

  static async verify(
    token: string,
    options?: VerifyOptions,
  ): Promise<Token | null> {
    try {
      const decodedToken = jwt.verify(
        token,
        this.secretKey,
        options,
      ) as TokenPayload;

      const expiration = jwt.decode(token, { json: true })?.exp ?? 0;
      const _this = new Token(
        token,
        expiration ? DateTime.fromSeconds(expiration) : DateTime.local(),
        decodedToken,
      );
      await _this.getData();

      return _this;
    } catch (error) {
      console.error("Error al verificar el token:", error);
      return null;
    }
  }

  static async getFromDB(id: string): Promise<null | Token> {
    return this.verify(id);
  }

  static async getTokenFrom(request: Request | null): Promise<Token | null> {
    if (request === null) return null;
    const auth = request.headers.authorization?.split(" ")[1];

    if (auth) {
      return Token.verify(auth);
    }

    return null;
  }

  async writeToDb(): Promise<void> {
    try {
      Database.run(
        "CREATE TABLE IF NOT EXISTS encrypted_data (token_id TEXT, expiration DATETIME, data TEXT)",
      );

      const data = this.dataToSave;
      const iv = Token.generateIV();
      const dataToEncrypt = JSON.stringify({ data }); // Guardar el IV con los datos
      const cipher = crypto.createCipheriv(this.algorithm, Token.key, iv);
      const encryptedData = Buffer.concat([
        cipher.update(dataToEncrypt, "utf-8"),
        cipher.final(),
      ]);
      const stmt = "INSERT INTO encrypted_data VALUES (?, ?, ?)";
      await Database.prepareRun(
        stmt,
        this.value,
        toDbDate(this.expiration),
        JSON.stringify({
          iv: iv.toString("hex"),
          encryptedData: encryptedData.toString("hex"),
        }),
      );
    } catch (error) {
      console.error("Error al escribir en la base de datos:", error);
    }
  }

  public getDataSaved(): any {
    try {
      if (this.dataEncripted) {
        const dataRaw = new MapValueCheck(JSON.parse(this.dataEncripted));
        const encryptedData = Buffer.from(
          dataRaw.nonEmptyStringOrFail("encryptedData"),
          "hex",
        );
        const iv = Buffer.from(dataRaw.nonEmptyStringOrFail("iv"), "hex");
        const decipher = crypto.createDecipheriv(this.algorithm, Token.key, iv);
        const decryptedData = Buffer.concat([
          decipher.update(encryptedData),
          decipher.final(),
        ]);
        const { data } = JSON.parse(decryptedData.toString("utf-8"));
        return data;
      }

      return null;
    } catch (error) {
      console.error("Error al obtener datos cifrados:", error);
      throw error; // Asegúrate de manejar el error en la capa superior si es necesario
    }
  }

  private async getData(): Promise<void> {
    try {
      const query = "SELECT data FROM encrypted_data WHERE token_id = ?";

      const row = await Database.get<{
        data: string;
        expiration: string;
      }>(query, this.value);

      this.dataEncripted = row.data;
    } catch (error) {
      console.error("Error al obtener datos cifrados:", error);
      throw error; // Asegúrate de manejar el error en la capa superior si es necesario
    }
  }

  public toApi(): Record<string, string> {
    return {
      value: this.value,
      auth: `Bearer ${this.value}`,
      expiration: this.expiration.toISO(),
    };
  }
}
