export function requestAnimationFrame(callback: () => void): () => void {
  const frameRate: number = 60; // Frames por segundo
  const interval: number = 1000 / frameRate;

  let lastTime: number | undefined;
  let requestId: NodeJS.Immediate | undefined;

  function loop(timestamp: number): void {
    if (!lastTime) {
      lastTime = timestamp;
    }

    const elapsed: number = timestamp - lastTime;

    if (elapsed >= interval) {
      callback();
      lastTime = timestamp - (elapsed % interval);
    }

    requestId = setImmediate(() => loop(timestamp));
  }

  requestId = setImmediate(() => loop(Date.now()));

  return (): void => {
    if (requestId) {
      clearImmediate(requestId);
    }
  };
}
