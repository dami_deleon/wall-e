function replacer(key: any, value: any) {
  if (value instanceof Map) {
    return {
      dataType: "Map",
      value: Array.from(value.entries()),
    };
  } else {
    return value;
  }
}

function reviver(key: any, value: any) {
  if (typeof value === "object" && value !== null) {
    if (value.dataType === "Map") {
      return new Map(value.value);
    }
  }
  return value;
}

export function objectToString(value: any): string {
  return JSON.stringify(value, replacer);
}

export function stringToObject<T>(value: any): T {
  return JSON.parse(value, reviver);
}
