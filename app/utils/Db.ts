import { DateTime } from "luxon";
import sqlite3 from "sqlite3";

const dbDateFormat = "yyyy-MM-dd HH:mm:ss";
const db = new sqlite3.Database("wall_e.db");

export function toDbDate(date: DateTime): string {
  return date.toUTC().toFormat(dbDateFormat);
}

export function fromDbDate(date: string): DateTime {
  return DateTime.fromFormat(date, dbDateFormat, {
    zone: "UTC",
  });
}

export class Database {
  static run(sql: string, params?: any[]): Promise<void> {
    return new Promise((resolve, reject) => {
      db.run(sql, params, function (err) {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  static get<T = any>(sql: string, ...params: any[]): Promise<T> {
    return new Promise((resolve, reject) => {
      db.get(sql, [...params], (err, row) => {
        if (err) {
          reject(err);
        } else {
          resolve(row as T);
        }
      });
    });
  }

  static all<T = any>(sql: string, params?: any[]): Promise<T[]> {
    return new Promise((resolve, reject) => {
      db.all(sql, params, (err, rows) => {
        if (err) {
          reject(err);
        } else {
          resolve(rows as T[]);
        }
      });
    });
  }

  static prepareRun(sql: string, ...params: any[]): Promise<void> {
    return new Promise((resolve, reject) => {
      const stmt = db.prepare(sql);

      stmt.run(params, function (err) {
        stmt.finalize(); // Finalizamos la consulta preparada después de ejecutarla

        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
}
