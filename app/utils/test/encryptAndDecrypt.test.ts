import { Token } from "../../model/Token"; // Asegúrate de importar correctamente la clase Token

let token_id;

// Función para cifrar y guardar en la base de datos
async function cifrarYGuardar() {
  // Datos a cifrar
  const dataToSave = "Hello, this is a secret!";

  // Crear un nuevo token
  const token = Token.create(
    { user: "testUser", app: "test" },
    { expiresIn: "1h" }, // Opciones de caducidad del token
  );

  token.setDataToSave(dataToSave);

  // Cifrar y guardar en la base de datos
  await token.writeToDb();
  // Mostrar información
  token_id = token.value;
}

// Función para descifrar y mostrar datos
async function descifrarYMostrar() {
  // Obtener el token de la base de datos (aquí deberías obtener el token adecuado)
  const tokenFromDb = await Token.verify(token_id); // Ejemplo, ajusta según tu implementación
  if (tokenFromDb === null)
    console.log("No se encontró el token en la base de datos.");

  console.log("token obtenido de la DB, correctamente");

  // Descifrar y obtener datos
  const datosEncriptados = tokenFromDb.getEncriptedData();

  if (!datosEncriptados) console.log("No existe datos encriptados.");

  // console.log("Datos encriptados:", datosEncriptados);

  const datosDesencriptados = await tokenFromDb.getDataSaved();

  if (!datosDesencriptados) console.log("No se pudo desencriptar.");

  console.log("Datos desencriptados:", datosDesencriptados);
}

// Ejecutar funciones
cifrarYGuardar().then(() => {
  // Luego de cifrar y guardar, descifrar y mostrar los datos
  descifrarYMostrar();
});
