import { join } from "path";
import Server from "./server";
import { app, BrowserWindow } from "electron";

export default class Walle {
  public mainWindow: BrowserWindow;
  public myIndexView: string;

  constructor() {
    this.myIndexView = join(__dirname, "..", "dist", "index.html");
  }

  private getEnvEncoded(): string {
    const envs = { apiPath: Server.init() };
    return encodeURIComponent(JSON.stringify(envs));
  }

  private initWindow() {
    this.mainWindow = new BrowserWindow({
      width: 1200,
      height: 800,
    });

    this.mainWindow.loadURL(`http://localhost:5173?${this.getEnvEncoded()}`);

    // this.mainWindow.loadURL(
    //   `file://${this.myIndexView}?${this.getEnvEncoded()}`,
    // );
  }

  public init(): void {
    app.whenReady().then(() => {
      this.initWindow();
    });

    app.on("window-all-closed", () => {
      app.quit();
    });

    app.on("second-instance", () => {
      if (this.mainWindow !== null) {
        if (this.mainWindow.isMinimized()) this.mainWindow.restore();
        this.mainWindow.focus();
      }
    });

    app.on("activate", () => {
      const allWindows = BrowserWindow.getAllWindows();
      if (allWindows.length) {
        allWindows[0].focus();
      } else {
        this.initWindow();
      }
    });
  }
}
