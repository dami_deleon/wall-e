import express, { type Express } from "express";
import cors from "cors";
import routes from "../http/api";

export default class Server {
  private static app: Express = express();
  private static port = 3174;

  public static init(): string {
    Server.app.use(cors());
    Server.app.use(express.json());

    Server.app.use("/api", routes);

    Server.app.listen(Server.port, () => {
      console.log(`[WALL-E] Listening on port: ${Server.port}`);
    });

    return `http://localhost:${Server.port}/api`;
  }
}
