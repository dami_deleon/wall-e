import { type Page, Browser, LaunchOptions } from "playwright";
import { DateTime } from "luxon";
import crypto from "crypto";
import * as path from "path";
import { promises as fs } from "fs";
import { requestAnimationFrame } from "../../utils/SimulateAnimationFrame";
import { chromium } from "playwright";
import { MapValueCheck } from "../../utils/MapValueCheck";

const loginPage =
  "https://liveconnect.chat/es/ilogin/validar/-/L2VzL2FkbS1wZXJmaWw=/";

const dashboardPage = "https://liveconnect.chat/es/adm-liveconnect";

const sessionDataFile = path.join(__dirname, "sessionData.json");

type Message = {
  name: string;
  channel: string;
  dateTime: string;
  prevText: string;
};

interface StorageState {
  cookies: Array<{
    name: string;

    value: string;

    domain: string;

    path: string;

    /**
     * Unix time in seconds.
     */
    expires: number;

    httpOnly: boolean;

    secure: boolean;

    sameSite: "Strict" | "Lax" | "None";
  }>;

  origins: Array<{
    origin: string;

    localStorage: Array<{
      name: string;

      value: string;
    }>;
  }>;
}

export class Liveconnect {
  constructor(
    private page: Page,
    private browser: Browser,
  ) {}

  public async closeBrowser(): Promise<void> {
    return await this.browser.close();
  }

  static async getInitBrowser(options?: LaunchOptions): Promise<Browser> {
    const playwright = await import("playwright");
    return playwright.chromium.launch(options);
  }

  static async initSession(
    user: string,
    password: string,
  ): Promise<Liveconnect> {
    console.log("Creando contexto");

    const browser = await this.allocBrowser(user, password);
    const loginPage = await this.loginToService(user, password, browser);
    return new Liveconnect(loginPage, browser);
  }

  private static async allocBrowser(user: string, password: string) {
    const storageState = await this.getStorageStateOrNull(user, password);

    console.log("Accediendo al servicio");
    const browser = await chromium.launch({
      headless: false,
    });

    if (storageState) {
      console.log("cargando con el contexto guardado");

      await browser.newContext({
        storageState,
      });
    }

    return browser;
  }

  async getReport(): Promise<Message[] | null> {
    const page = this.page;
    await this.waitAndHandleLoaders(page);
    console.log("Agaurdando por la lista de mensajes");
    await page.waitForSelector(".conversations--item", { state: "visible" });
    console.log("Viendo lista de chats activos");
    await page.click("#inbox-activas a");

    console.log("Viendo mensajes");
    const result: Array<object> = await page.evaluate(() => {
      console.log("conversations-scroll-area");

      const duration = 15000;

      return new Promise((resolve, reject) => {
        const element: HTMLElement | null = document.getElementById(
          "conversations-scroll-area",
        );
        if (element === null) {
          reject(
            "no se encuentra el contenedor de mensajes #conversations-scroll-area",
          );
        }

        const messages = new Map(); // Mapa para almacenar los datos
        element!.scrollTop = 0;
        const start = 0;
        const target = element!.scrollHeight - element!.clientHeight;
        let currentTime = 0;
        const incrementAmount = 20;

        function easeInOutQuad(t, b, c, d) {
          t /= d / 2;
          if (t < 1) return (c / 2) * t * t + b;
          t--;
          return (-c / 2) * (t * (t - 2) - 1) + b;
        }

        function animateScroll() {
          currentTime += incrementAmount;
          const val = easeInOutQuad(
            currentTime,
            start,
            target - start,
            duration,
          );

          element!.scrollTop = val;

          Array.from(element!.querySelectorAll(".conversations--item")).forEach(
            (e) => {
              const id = e.getAttribute("id");
              const nameElement = e.querySelector(".name");
              const channelElement = e.querySelector(".canal");
              const timeAgoElement = e.querySelector(".roll");
              const dateTimeElement = e.querySelector(".over");
              const prevTextElemnt = e.querySelector(".prevtext");

              if (
                id &&
                nameElement &&
                channelElement &&
                timeAgoElement &&
                dateTimeElement
              ) {
                const name = nameElement!.textContent!.trim();
                const channel = channelElement!.textContent!.trim();
                const dateTime = dateTimeElement!.textContent!.trim();
                const prevText = prevTextElemnt!.textContent!.trim();

                messages.set(id, {
                  name,
                  channel,
                  dateTime, // 2 de enero de 2024 4:55pm
                  prevText,
                });
              }
            },
          );

          if (currentTime < duration) {
            requestAnimationFrame(animateScroll);
          } else {
            resolve(Array.from(messages.values()));
          }
        }

        animateScroll();
      });
    });
    if (result.length > 0) {
      console.log("Creando reporte");
      return result.map((obj) => {
        const raw = new MapValueCheck(obj);
        const dateTime = raw.nonEmptyStringOrFail("dateTime");

        return {
          name: raw.stringOrNull("name") ?? "no name",
          channel: raw.nonEmptyStringOrFail("channel"),
          dateTime: Liveconnect.dateFromTextOrNull(dateTime).toISO(), // 2 de enero de 2024 4:55pm
          prevText: raw.stringOrNull("prevText") ?? "no text",
        } as Message;
      });
    }

    return null;
  }

  private async waitAndHandleLoaders(page: Page) {
    console.log("Esperando a que la página haya cargado completamente");
    await page.waitForLoadState();

    console.log("Esperando a que el loader desaparezca");
    await page.waitForSelector("div.loader-pge", { state: "hidden" });

    console.log("Ver si aparece el overlay de bienvenida");
    let chardinMask: boolean = null;
    try {
      chardinMask = (await page.waitForSelector("#chardin-mask", {
        timeout: 4000,
      }))
        ? true
        : false;
    } catch (error) {
      console.log("El elemento #chardin-mask no se encontró.");
    }

    if (chardinMask) {
      console.log(
        "El elemento #chardin-mask existe. Buscando y haciendo clic en el botón de cierre...",
      );

      // Busca el botón de cierre dentro de #chardin-mask
      const closeButton = await page.waitForSelector(".btn-close", {
        state: "visible",
      });

      closeButton.click();
    } else {
      console.log("El elemento #chardin-mask no existe en la página.");
    }
  }

  private static async getStorageStateOrNull(
    user: string,
    password: string,
  ): Promise<StorageState | null> {
    return await this.loadStorageState(user, password);
  }

  private static async loginToService(
    user: string,
    password: string,
    browser: Browser,
  ): Promise<Page> {
    const page = await browser.newPage();

    await page.goto(loginPage);
    await page.route("**/*.{png,jpg,jpeg}", (route) => route.abort());
    await page.fill('input[name="user"]', user);
    await page.fill('input[name="pass"]', password);
    console.log('Haciendo clic en el botón "Iniciar sesión"');
    await page.getByRole("button", { name: "Iniciar sesión" }).click();
    console.log("Esperando a que la página haya cargado completamente");
    await page.waitForLoadState();
    console.log("Verificando la url del dashboard");
    if (page.url() !== dashboardPage) throw Error("No se pudo iniciar sesión");
    return page;
  }

  private static async saveBrowserContext(
    user: string,
    password: string,
    contextState: StorageState,
  ): Promise<void> {
    const key = this.getKey(user, password);

    console.log("Datos encriptados");
    const dataToWrite = JSON.stringify({
      key: key,
      contextState,
    });

    await fs.writeFile(sessionDataFile, dataToWrite, "utf-8");
  }

  private static async loadStorageState(
    user: string,
    password: string,
  ): Promise<StorageState | null> {
    try {
      const dataFile = await fs.readFile(sessionDataFile, "utf-8");

      if (!dataFile) {
        console.log("No hay contexto guardado");
        return null;
      }

      console.log("Desencriptando datos");
      const { key: keyString, contextState } = JSON.parse(dataFile);

      const key = this.getKey(user, password);
      if (key === keyString) {
        console.log("Datos desencriptados");
        return contextState as StorageState;
      } else {
        return null;
      }
    } catch (error) {
      console.error("Error al cargar la sesión encriptada:", error);
      return null;
    }
  }

  static getKey(user: string, password: string): string {
    const hash = crypto.createHash("sha256");
    hash.update(`${user}${password}`);
    return hash.digest("hex");
  }

  private static dateFromTextOrNull(text: string): null | DateTime {
    const regex = /(\d{1,2}) de (\w+) de (\d{4}) (\d{1,2}):(\d{2})([ap]m)/;
    const match = text.match(regex);

    if (!match) throw Error("No fue posible parsear la fecha: " + text);

    const [, day, monthName, year, hour, minute, meridian] = match;
    const month = DateTime.fromFormat(monthName, "MMMM", {
      locale: "es",
    }).month;
    const date = DateTime.fromObject({
      day: Number(day),
      month: Number(month),
      year: Number(year),
      hour: meridian === "pm" ? Number(hour) + 12 : Number(hour),
      minute: Number(minute),
    });

    return date;
  }
}
