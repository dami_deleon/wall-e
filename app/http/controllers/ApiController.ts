import { Request, Response } from "express";
import { DateTime } from "luxon";
import { Token } from "../../model/Token";

export default class ApiController {
  constructor(
    protected request: Request,
    protected response: Response,
  ) {}

  protected async apiSuccess(
    data: object = {},
    meta: object = {},
    status: number = 200,
  ): Promise<Response> {
    return this.response.status(status).json({
      meta: {
        now: DateTime.now().toISO(),
        status,
        ...meta,
      },
      data,
    });
  }

  protected async apiError(
    message: string,
    status: number = 500,
  ): Promise<Response> {
    return this.response.status(status).json({
      meta: {
        now: DateTime.now().toISO(),
        status,
        message,
      },
    });
  }

  protected async dispatchApiRequest(
    callback: (
      apiController: ApiController,
      request: Request,
    ) => Promise<Response>,
  ): Promise<Response> {
    try {
      return await callback(this, this.request);
    } catch (error) {
      console.error(error);
      return this.apiError(error);
    }
  }

  protected async getAuthToken(): Promise<Token | null> {
    return Token.getTokenFrom(this.request);
  }
}
