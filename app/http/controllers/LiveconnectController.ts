import { Request, Response } from "express";
import ApiController from "./ApiController";
import { Liveconnect } from "../services/Liveconnect";
import { MapValueCheck } from "../../utils/MapValueCheck";
import { Token } from "../../model/Token";

const appRouterName = "liveconnect";

export class LiveconnectController extends ApiController {
  constructor(request: Request, response: Response) {
    super(request, response);
  }

  public async test(): Promise<Response> {
    try {
      // Utiliza el método apiSuccess de ApiController
      return this.apiSuccess(
        { message: "Ejemplo obtenido con éxito" },
        {
          token: "el purete",
        },
      );
    } catch (error) {
      console.error(error);
      // Utiliza el método apiError de ApiController
      return this.apiError("Error interno del servidor");
    }
  }

  public async connect(): Promise<Response> {
    const { body } = this.request;
    const bodyValueCheck = new MapValueCheck(body);

    const user = bodyValueCheck.nonEmptyStringOrFail("user");
    const password = bodyValueCheck.nonEmptyStringOrFail("password");

    const liveconnect = await Liveconnect.initSession(user, password);

    console.log("Creando token");
    const token = Token.create({
      user: user,
      app: appRouterName,
    });

    token.setDataToSave({ user, password });
    token.writeToDb();

    const data = await liveconnect.getReport();

    liveconnect.closeBrowser();

    return this.apiSuccess({
      user,
      token: token.toApi(),
      report: data,
    });
  }

  public async getReport(): Promise<Response> {
    const token = await this.getAuthToken();
    if (!token) this.apiError("Token inválido", 401);

    const rawDataSaved = new MapValueCheck(token.getDataSaved() as object);

    const user = rawDataSaved.nonEmptyStringOrFail("user");
    const password = rawDataSaved.nonEmptyStringOrFail("password");
    const liveconnect = await Liveconnect.initSession(user, password);
    const data = await liveconnect.getReport();

    liveconnect.closeBrowser();

    return this.apiSuccess({
      report: data,
    });
  }
}
