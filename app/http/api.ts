import { Request, Response, Router } from "express";
import { LiveconnectController } from "./controllers/LiveconnectController";

const router = Router();
const liveconnectRoutes = Router();

liveconnectRoutes
  .get("/test", (req: Request, res: Response) => {
    const liveconnectController = new LiveconnectController(req, res);
    return liveconnectController.test();
  })
  .post("/connect", (req: Request, res: Response) => {
    const liveconnectController = new LiveconnectController(req, res);
    return liveconnectController.connect();
  })
  .get("/report", (req: Request, res: Response) => {
    const liveconnectController = new LiveconnectController(req, res);
    return liveconnectController.getReport();
  });

router
  .all("/", () => {
    console.log("llega");
  })
  .use("/liveconnect", liveconnectRoutes);

export default router;
